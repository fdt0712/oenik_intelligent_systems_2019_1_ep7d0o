## Dog or Cat image organizer

**Table Of Contents**

[TOC]

### About the Project
This project is created for my Intelligent Systems Subject at Obuda Univercity.
### Project Description
At home I've a server running, which hosts owncloud as my private cloud.
It also has a mobile app, which can upload the images taken on my phone to owncloud.

This project would have a python script utilizing tensorflow and keras to guess if my uploaded image is a cat or a dog and move the image right into the specified folder(We have both a dog and a cat, so it is quiet handy to move the images automatically into the right folder.).

This would require to create a Convolutional Neural Network and apply some hooks to owncloud.

#### Why Convolutional Neural Network?
According to [Wikipedia](https://en.wikipedia.org/wiki/Convolutional_neural_network):
> In machine learning, a convolutional neural network is a class of deep, feed-forward artificial neural networks, most commonly applied to analyzing visual imagery.

The Convolutional Neural Network is useful for detecting shapes and more complicated parts of images.

For more detailed description [read more here](https://www.quora.com/How-do-convolutional-neural-networks-work).

### Tools
 - [Tensorflow](https://www.tensorflow.org/)
 - [Keras](https://keras.io/)
 - [Jupyter](https://jupyter.org/)

### Resources
 - [Cats and Dogs Dataset](https://www.microsoft.com/en-us/download/confirmation.aspx?id=54765)
 - [Navigation and Pre-App configuration](https://doc.owncloud.org/server/8.2/developer_manual/app/init.html)
 - [How to Ownlocud hook](https://doc.owncloud.org/server/8.2/developer_manual/app/hooks.html)
 - [Example](https://stackoverflow.com/questions/33531730/owncloud-hooks)
 - [Docker image](https://github.com/lspvic/jupyter_tensorboard/tree/master/docker)

### Implementation Results
After preparing the training dataset I run the training with 0 dense layers, 3 convolutional layers and 64 as layer size.
This can be altered by modifying the arrays in the train notebook.

After training the network we can see the following results:
![TensorBoard](./screenshots/tensorboard.png "TensorBoard")


Once I got the first model I run some tests on the images from the `work/test_dataset` directory.
![Test Results](./screenshots/loop_result.png "Test Results")

### Conclusion
The network is working, but for everyday use I need to tweak it more to get better results so I can use it with my cloud. Also note, that I still not trained with my cat and dog images so it can be improved, but since the lack of time and slow hardware this was all I could do for now.
Will be improved and if working at least 90% accuracy I will use this on my cloud for organizing my images a bit.

Unfortunately, I had no time to implement the owncloud hook.

### Running inside docker
I prepared a docker-compose.yaml file, so by running `$ docker-compose build` we can build the images and by `$ docker-compose up` we can start the container.

Once container is started , you should be able to find the interfaces to manage jupyter and tensorboard instances in [localhost:8888](http://localhost:8888).

To log into bash prompt of the container use `$ docker-compose run oe_tensorflow bash`
