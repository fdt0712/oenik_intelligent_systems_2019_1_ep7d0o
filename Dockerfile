FROM jupyter/tensorflow-notebook

MAINTAINER diffy0712 <ferencz.david@protonmail.com>

RUN pip install --pre \
    jupyter-tensorboard \
    opencv-python \
    tqdm